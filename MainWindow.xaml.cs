﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFTextEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string filename = "";
        public string opened_filename = "";

        public MainWindow()
        {
            InitializeComponent();
            textBox.Clear();
            // Add key Binding for New command
            RoutedCommand newCmdNew = new RoutedCommand();
            newCmdNew.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmdNew, MenuItem_New));
            // Add key Binding for Open command
            RoutedCommand newCmdOpen = new RoutedCommand();
            newCmdOpen.InputGestures.Add(new KeyGesture(Key.O, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmdOpen, MenuItem_Open));
            // Add key Binding for Save command
            RoutedCommand newCmdSave = new RoutedCommand();
            newCmdSave.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmdSave, MenuItem_Save));
            // Add key Binding for Exit command
            RoutedCommand newCmdExit = new RoutedCommand();
            newCmdExit.InputGestures.Add(new KeyGesture(Key.Q, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmdExit, MenuItem_Exit));
        }

        #region File Menu Items 
        private void MenuItem_New(object sender, RoutedEventArgs e)
        {
            opened_filename = "";
            textBox.Clear();
        }
            // Open through Menu Item
        private void MenuItem_Open(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg_open = new Microsoft.Win32.OpenFileDialog();
            dlg_open.FileName = ""; // Default file name
            dlg_open.DefaultExt = ".txt"; // Default file extension
            dlg_open.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = dlg_open.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                opened_filename = dlg_open.FileName;
                if (opened_filename == string.Empty) return;

                try
                {
                    var Reader = new System.IO.StreamReader(opened_filename, Encoding.GetEncoding(1251));
                    textBox.Text = Reader.ReadToEnd();
                }
                catch(System.IO.FileNotFoundException file_not_found)
                {
                    MessageBox.Show(file_not_found.Message + "\n File Not Found");
                }
                
            }
        }

        private void MenuItem_Save_As(object sender, RoutedEventArgs e)
        {
            Internal_Saver();
        }

        private void MenuItem_Save(object sender, RoutedEventArgs e)
        {
            if (opened_filename != string.Empty)
            {
                try
                {
                    var Writer = new System.IO.StreamWriter(opened_filename, false, Encoding.GetEncoding(1251));
                    Writer.Write(textBox.Text);
                    Writer.Close();
                }
                catch (Exception exc_writer)
                {
                    MessageBox.Show(exc_writer.Message, "Error", MessageBoxButton.OK);
                }
            }   
            else
            {
                Internal_Saver();
            }
        }

        private void Internal_Saver()
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg_save = new Microsoft.Win32.SaveFileDialog();
            dlg_save.FileName = ""; // Default file name
            dlg_save.DefaultExt = ".txt"; // Default file extension
            dlg_save.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg_save.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg_save.FileName;
                try
                {
                    var Writer = new System.IO.StreamWriter(filename, false, Encoding.GetEncoding(1251));
                    Writer.Write(textBox.Text);
                    Writer.Close();
                }
                catch (Exception exc_writer)
                {
                    MessageBox.Show(exc_writer.Message, "Error", MessageBoxButton.OK);
                }

            }
        }

        private void MenuItem_Exit(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        #endregion

        #region Style Menu Items

        private void MenuItem_Style_Olive(object sender, RoutedEventArgs e)
        {
            textBox.Background = Brushes.Olive;
        }

        private void MenuItem_Style_Default(object sender, RoutedEventArgs e)
        {
            textBox.Background = Brushes.White;
        }

        private void MenuItem_Style_SpringGreen(object sender, RoutedEventArgs e)
        {
            textBox.Background = Brushes.MediumSpringGreen;
        }

        #endregion

    }
}
